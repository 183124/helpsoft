<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Solicitacao;
use App\Models\Avaliacao;
use Illuminate\Support\Facades\Auth;

class AvaliacaoController extends Controller
{
        public function create($id) {
        return view('avaliacoes.create', compact('id'));
    }

    public function store(Request $request) {
        $nova_avaliacao = $request->all();
        Avaliacao::create($nova_avaliacao);
        return redirect()->route('avaliacoes');
    }

    public function index(){
        $avaliacoes = Solicitacao::where('status', 'Concluido')->get();
       // dd(Solicitacao::with('avaliacao')->get());
        return view('avaliacoes.index', ['avaliacoes'=>$avaliacoes]);
    }
}
