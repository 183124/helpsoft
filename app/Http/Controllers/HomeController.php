<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Solicitacao;
use App\Models\Avaliacao;
use App\Models\Servico;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();

         if ($user->cargos->id == 1) {
            $minhas = Solicitacao::where('atendente_id', $user->id)->get();
            $servicos = Servico::all();
            $disponiveis = Solicitacao::where('atendente_id', null)->get();
            return view('home', ['user'=>$user, 'minhas'=>$minhas, 'servicos'=>$servicos, 'disponiveis'=>$disponiveis]);
         }
         else {
            $solicitacoes = Solicitacao::where('user_id', $user->id)->get();
            $avaliacoes = Solicitacao::where('status', 'Concluido')->where('user_id', $user->id)->get();
            return view('home', ['solicitacoes'=>$solicitacoes, 'avaliacoes'=>$avaliacoes, 'user'=>$user]);
         }
    }
}
