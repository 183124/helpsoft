<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;


class PDFController extends Controller
{
    public function indexAtendente(){
        return view('relatorios.indexAtendente');
    }

    public function indexUser(){
        return view('relatorios.indexUsuario');
    }
}
