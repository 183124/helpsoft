<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Relato;
use App\Models\Solicitacao;

class RelatoController extends Controller
{
    public function create($id) {
        return view('relatos.create', compact('id'));
    }

    public function store(Request $request) {
        $novo_relato = $request->all();
        Relato::create($novo_relato);
        return redirect()->route('solicitacoes.indexAtendente');
    }
    
    public function mostrar($id){
        $relatos = Relato::where('solicitacao_id', $id)->get();
        return view('relatos.mostrar', ['relatos'=>$relatos, 'id'=>$id]);
    }

    public function view($id){
        $relatos = Relato::where('solicitacao_id', $id)->get();
        return view('relatos.view', ['relatos'=>$relatos]);
    }
}