<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Servico;

class ServicoController extends Controller
{
    public function index() {
        $servicos = Servico::orderBy('tipo')->paginate(5);
        return view('servicos.index', ['servicos'=>$servicos]);
    }

    public function create() {
        return view('servicos.create');
    }

    public function store(Request $request) {
        $novo_servico = $request->all();
        Servico::create($novo_servico);
        return redirect()->route('servicos');
    }

    public function destroy($id){
        Servico::find($id)->delete();
        return redirect()->route('servicos');
    }

    public function edit($id){
        $servico = Servico::find($id);
        return view('servicos.edit', compact('servico'));
    }

    public function update(Request $request, $id){
        Servico::find($id)->update($request->all());
        return redirect()->route('servicos');
    }

}
