<?php

namespace App\Http\Controllers;

use App\Models\Avaliacao;
use Illuminate\Http\Request;
use App\Models\Solicitacao;
use App\Models\Servico;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

use Barryvdh\DomPDF\Facade\Pdf;


class SolicitacaoController extends Controller
{
    private $avaliacao;

    public function __construct(Avaliacao $avaliacao)
    {
        $this->avaliacao = $avaliacao;
    }

    public function indexUser()
    {
        $user = Auth::user();

        
           
            $solicitacoes = Solicitacao::where('user_id', $user->id)->get();
            return view('solicitacoes.user_index', ['solicitacoes' => $solicitacoes]);
        
    }

    public function indexAtendente()
    {
        $user = Auth::user();

        
            $solicitacoes = Solicitacao::where('atendente_id', $user->id)->get();
            return view('solicitacoes.atendente_index', ['solicitacoes' => $solicitacoes]);
        
            
        
    }

    public function disponiveis()
    {
        $solicitacoes = Solicitacao::where('atendente_id', null)->get();
        return view('solicitacoes.disponiveis', ['solicitacoes' => $solicitacoes]);
    }

    public function create()
    {
        $user = Auth::user();
        return view('solicitacoes.create', compact('user'));
    }

    public function store(Request $request)
    {
        $nova_solicitacao = $request->all();
        $solicitacao = Solicitacao::create($nova_solicitacao);

//        $this->avaliacao->create([
//            'solicitacao_id' => $solicitacao->id,
//            'create_at' => now()
//        ]);

$user = Auth::user();
if ($user->cargos->id == 1) {
    return redirect()->route('solicitacoes.indexAtendente');
}
else {return redirect()->route('solicitacoes.indexUser');}
    }

    public function destroy($id)
    {
        Solicitacao::find($id)->delete();
        
        $user = Auth::user();
        if ($user->cargos->id == 1) {
            return redirect()->route('solicitacoes.indexAtendente');
        }
        else {return redirect()->route('solicitacoes.indexUser');}
    }

    public function edit($id)
    {
        $solicitacao = Solicitacao::find($id);
        return view('solicitacoes.edit', compact('solicitacao'));
    }

    public function view($id)
    {
        $solicitacao = Solicitacao::find($id);
        return view('solicitacoes.view', compact('solicitacao'));
    }


    public function relatorioAtendente()
    {
        $users = User::where('cargo_id', 1)->get();
        $pdf = PDF::loadView('relatorios.atendente', ['users' => $users]);
        return $pdf->stream();

    }

    public function relatorioServicos()
    {   
        
        $user = Auth::user();
        if ($user->cargos->id == 1) {
            $servicos = Servico::all();
        }
        else {
            $servicos = Servico::all();
        }
        $pdf = PDF::loadView('relatorios.servicos', ['servicos' => $servicos]);
        return $pdf->stream();

    }

    public function relatorioNotas()
    {
        
        $user = Auth::user();
        if ($user->cargos->id == 1) {
            $notas = Avaliacao::all();
        }
        else {
            $notas = Avaliacao::all();
        }
        $pdf = PDF::loadView('relatorios.notas', ['notas' => $notas]);
        return $pdf->stream();

    }

    public function aceitar($id)
    {
        $user = Auth::user();
        $solicitacao = Solicitacao::find($id);
        return view('solicitacoes.aceitar', compact('solicitacao', 'user'));
    }

    public function relato($id)
    {
        $solicitacao = Solicitacao::find($id);
        return view('solicitacoes.edit', compact('solicitacao'));
    }

    public function update(Request $request, $id)
    {
        Solicitacao::find($id)->update($request->all());
        $user = Auth::user();
        if ($user->cargos->id == 1) {
            return redirect()->route('solicitacoes.indexAtendente');
        }
        else {return redirect()->route('solicitacoes.indexUser');}
    }
}
