<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $usuarios = User::orderBy('id')->paginate(5);
        return view('usuarios.index', ['usuarios'=>$usuarios]);
    }

    public function destroy($id){
        User::find($id)->delete();
        return redirect()->route('usuarios');
    }

    public function edit($id){
        $usuario = User::find($id);
        return view('usuarios.edit', compact('usuario'));
    }

    public function update(Request $request, $id){
        User::find($id)->update($request->all());
        return redirect()->route('usuarios');
    }
}
