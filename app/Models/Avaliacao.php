<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Avaliacao extends Model
{
    protected $table = "avaliacoes";
    protected $fillable = ['prazo', 'atendimento', 'resultado', 'solicitacao_id'];

    public function solicitacao() {
        return $this->belongsTo("App\Models\Solicitacao");
    }

}
