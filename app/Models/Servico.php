<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Servico extends Model
{
    use HasFactory;

    protected $table = "servicos";
    protected $fillable = ['tipo','descricao'];

    public function solicitacoes() {
        return $this->hasMany("App\Models\Solicitacao");
    }
}
