<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Solicitacao extends Model
{
    use HasFactory;

    protected $table = "solicitacoes";
    protected $fillable = ['descricao', 'gravidade', 'status', 'data_abertura', 'data_fechamento', 'servico_id', 'user_id', 'atendente_id'];

    public function servico() {
        return $this->belongsTo("App\Models\Servico");
    }

    public function user() {
        return $this->belongsTo("App\Models\User");
    }

    public function avaliacao() {
        return $this->hasOne(Avaliacao::class);
    }
}

