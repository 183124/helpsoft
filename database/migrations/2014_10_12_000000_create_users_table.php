<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->unsignedBigInteger('cargo_id')->default(1);
            $table->foreign('cargo_id')->references('id')->on('cargos');
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

        DB::table('cargos')->insert(
            ['nome' => 'Atendente', 'descricao' => 'Atendente do sistema']
        );
        DB::table('cargos')->insert(
            ['nome' => 'Usuario', 'descricao' => ' Usuario da empresa']
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
