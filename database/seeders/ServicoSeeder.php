<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Servico;

class ServicoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Servico::create(['tipo' => 'Instalação de Software', 'descricao' => 'teste']);
        Servico::create(['tipo' => 'Analise de Harware', 'descricao' => 'teste']);
        Servico::create(['tipo' => 'Troca de Peças', 'descricao' => 'teste']);
        Servico::create(['tipo' => 'Duvida Sobre Sistema', 'descricao' => 'teste']);      
    }
}
