@extends('adminlte::page')

@section ('content')
    <h3>Avalie o Atendimento com Valores de 1 a 5</h3>
    {!! Form::open(['route'=>'avaliacoes.store']) !!}

        <div class="form-group">
            {!! Form::label('prazo', 'Avalie o prazo em que a solicitação foi concluída:') !!}
            {!! Form::number('prazo', 1, ['class'=>'form-control', 'min' => 1, 'max' => 5]) !!}
        </div>
        <div class="form-group">
            {!! Form::label('atendimento',  'Avalie seu atendente') !!}
            {!! Form::number('atendimento', 1, ['class'=>'form-control', 'required' , 'min' => 1, 'max' => 5]) !!}
        </div>
        <div class="form-group">
            {!! Form::label('resultado',  'Avalie o Resultado da sua Solicitação') !!}
            {!! Form::text('resultado', 1, ['class'=>'form-control', 'required' , 'min' => 1, 'max' => 5]) !!}
        </div>
        <div class="form-group">
            {!! Form::label('solicitacao_id',  'Solicitacao de Origem') !!}
            {!! Form::text('solicitacao_id', $id, ['class'=>'form-control', 'required', 'readonly']) !!}
        </div>
        <div class='form-group'>
            {!! Form::submit('Concluir', ['class'=>'btn btn-primary'])!!}
            {!! Form::reset('Limpar', ['class'=>'btn btn-default']) !!}
        </div>
        
    {!! Form::close() !!}
@Stop