@extends('layouts.default')

@section('content')
    <h1>Solicitações Concluídas</h1>
    <table class="table table-stripe table-borered table-hover">
        <thead>
        <th>Descricao</th>
        <th>Status</th>
        <th>Ações</th>
        </thead>
        <tbody>
        @foreach($avaliacoes as $avaliacao)
            <tr>
                <td>{{ $avaliacao->descricao }}</td>
                <td>{{ $avaliacao->status }}</td>
                <td>
                    <a @if(!empty($avaliacao->avaliacao->resultado)) 'disabled' @else href="{{ route('avaliacoes.create', ['id'=>$avaliacao->id] ) }}"  @endif
                       class="btn-sm btn-{{!empty($avaliacao->avaliacao->resultado) ? 'success' : 'info' }}">
                        {{!empty($avaliacao->avaliacao->resultado)? 'Avaliado' : 'Avaliar' }}
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@stop


@section('table-delete')
    "relatos"
@stop
