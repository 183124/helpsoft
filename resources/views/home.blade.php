@extends('layouts.default')
@section('content')
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
    <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css">
    <link rel="stylesheet" href="dist/css/adminlte.min.css">
    <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="plugins/summernote/summernote-bs4.min.css">

    <h1 style="padding: 15px; text-align: center"> Bem Vindo, {{ Auth::user()->name }} </h1>

    @can('atendente')
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-6 col-6">
                        <div class="small-box bg-info">
                            <div class="inner">
                                <h2>{{ $minhas->count() }}</h2>
                                <p>Minhas Solicitações</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-ios-calculator"></i>
                            </div>
                            <a href="{{ route('solicitacoes.indexAtendente') }}" class="small-box-footer">Ver Minhas Solicitações<i
                                    class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-6">
                        <div class="small-box bg-success">
                            <div class="inner">
                                <h2>{{ $disponiveis->count() }}</h2>
                                <p>Solicitações Aguardando Apropriação</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-stats-bars"></i>
                            </div>
                            <a href="{{ route('solicitacoes.disponiveis') }}" class="small-box-footer">Ver
                                Solicitações<i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-6">
                        <div class="small-box bg-warning">
                            <div class="inner">
                                <h2>{{ $servicos->count() }}</h2>

                                <p>Serviços</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-android-people"></i>
                            </div>
                            <a href="{{ route('servicos') }}" class="small-box-footer">Ver Serviços<i
                                    class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endcan

    @can('usuario')
        @if($user->cargo_id == 2)
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6 col-6">
                            <div class="small-box bg-success">
                                <div class="inner">
                                    <h2>{{ (!empty($solicitacoes)) ? $solicitacoes->count() :''}}</h2>
                                    <p>Minhas Solicitacoes</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-stats-bars"></i>
                                </div>
                                <a href="{{ route('solicitacoes.indexUser') }}" class="small-box-footer">Ver Solicitacoes<i
                                        class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-6 col-6">
                            <div class="small-box bg-warning">
                                <div class="inner">
                                    <h2>{{ (!empty($avaliacoes)) ? $avaliacoes->count() :''}}</h2>
                                    <p>Solicitacoes Concluídas</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-stats-bars"></i>
                                </div>
                                <a href="{{ route('avaliacoes') }}" class="small-box-footer">Ver Solicitacoes<i
                                        class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>

                    </div>
                </div>

            </section>
        @endif
    @endcan
@stop
