<!DOCTYPE html>
<html>
    @php
    use App\Models\Solicitacao;
@endphp
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Relatório de Atendente</title>
</head>

<body>
    <h1
        style="text-align:center; font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif; text-transform: uppercase">
        Relatório de Atendente</h1>

    <hr size="2.25" noshade>
    
    <table class="table table-striped table-bordered" style="width:100%;">
        <thead style="background-color: grey;">
            <th>Nome</th>
            <th>Solicitações Abertas</th>
            <th>Solicitações Concluídas</th>
            <th>Solicitações Encerradas</th>
        </thead>
        <tbody>
            @foreach ($users as $user)
                    <tr style="text-align:center; font-size: 14px;">
                        @php

                        @endphp
                        <td>{{ $user->name }}</td>
                        <td>{{ Solicitacao::where('atendente_id', $user->id)->where('status', 'Andamento')->count() }}</td>
                        <td>{{ Solicitacao::where('atendente_id', $user->id)->where('status', 'Concluido')->count() }}</td>
                        <td>{{ Solicitacao::where('atendente_id', $user->id)->where('status', 'Encerrado')->count() }}</td>
                    </tr>
            @endforeach
        </tbody>
    </table>
</body>

</html>