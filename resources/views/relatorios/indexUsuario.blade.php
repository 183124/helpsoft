@extends('layouts.default')

@section('content')
    <br>

    <div style="text-align: center; padding: 2%">


        <div>
            <h2 style="text-transform: uppercase">Relatórios</h2>
            <hr size="2" width="50%">
            <a class="btn btn-outline-dark" href="{{ route('relatorios.servicos') }}" target="_blank">Serviços mais Solicitados</a>
            <br><br>
            <a class="btn btn-outline-dark" href="{{ route('relatorios.notas') }}" target="_blank">Avalições</a>
            <br><br>

        </div>


    </div>
@stop
