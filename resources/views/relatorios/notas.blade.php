<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Relatório de Satisfação</title>
</head>

<body>
    <h1
        style="text-align:center; font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif; text-transform: uppercase">
        Relatório de Satisfação</h1>

    <hr size="2.25" noshade>
    
    <table class="table table-striped table-bordered" style="width:100%;">
        <thead style="background-color: grey;">
            <th>Descrição</th>
            <th>Nota de Prazo</th>
            <th>Nota de Atendimento</th>
            <th>Nota de Resultado</th>
        </thead>
        <tbody>
            @foreach ($notas as $nota)
                        <tr style="text-align:center; font-size: 14px; color: green">
                            <td>{{ $nota->solicitacao->descricao }}</td>
                            <td>{{ $nota->prazo }}</td>
                            <td>{{ $nota->atendimento }}</td>
                            <td>{{ $nota->resultado }}</td>
                        </tr>
            @endforeach
        </tbody>
    </table>
</body>

</html>

