
<!DOCTYPE html>

<html>
@php
    use App\Models\Solicitacao;
@endphp

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Relatório de Serviços</title>
</head>

<body>
    <h1
        style="text-align:center; font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif; text-transform: uppercase">
        Relatório de Serviços</h1>

    <hr size="2.25" noshade>
    
    <table class="table table-striped table-bordered" style="width:100%;">
        <thead style="background-color: grey;">
            <th>Tipo</th>
            <th>Descricao</th>
            <th>Solicitações Deste Tipo</th>
        </thead>
        <tbody>
            @foreach ($servicos as $servico)
                        <tr style="text-align:center; font-size: 14px; color: green">
                            <td>{{ $servico->tipo }}</td>
                            <td>{{ $servico->descricao }}</td>
                            <td>{{ Solicitacao::where('servico_id',$servico->id)->count() }}</td>
                        </tr>
            @endforeach
        </tbody>
    </table>
</body>

</html>

