@extends('adminlte::page')

@section ('content')
    <h3>Novo Relato</h3>
    {!! Form::open(['route'=>'relatos.store']) !!}

        <div class="form-group">
            {!! Form::label('relato', 'Relato: ') !!}
            {!! Form::text('relato', null, ['class'=>'form-control', 'required']) !!}
        </div>
        <br>
        <div class="form-group">
            {!! Form::label('data', 'Data do Relato: ') !!}
            {!! Form::date('data', null, ['class'=>'form-control', 'required']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('solicitacao_id',  'Solicitacao de Origem') !!}
            {!! Form::text('solicitacao_id', $id, ['class'=>'form-control', 'required', 'readonly']) !!}
        </div>
        <div class='form-group'>
            {!! Form::submit('Criar Relato', ['class'=>'btn btn-primary'])!!}
            {!! Form::reset('Limpar', ['class'=>'btn btn-default']) !!}
        </div>
        
    {!! Form::close() !!}
@Stop