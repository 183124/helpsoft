@extends('layouts.default')

@section('content')
<h1>Relatos</h1>
<table class="table table-stripe table-borered table-hover">
    <thead>
        <th>Relato</th>
        <th>Data do Relato</th>
    </thead>
    <tbody>
        @foreach($relatos as $relato)
        <tr>
            <td>{{ $relato->relato }}</td>
            <td>{{Carbon\Carbon::parse( $relato->data )->format('d/m/Y')}}</td>           
        </tr>
        @endforeach
    </tbody>
</table>
<a href= "{{ route('relatos.create', ['id'=>$id] ) }}" class="btn-sm btn-success">Criar Relato</a>
@stop

@section('table-delete')
"relatos"
@stop