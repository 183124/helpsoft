@extends('adminlte::page')

@section ('content')
    <h3>Novo Servico</h3>
    {!! Form::open(['route'=>'servicos.store']) !!}
        <div class="form-group">
            {!! Form::label('tipo', 'Tipo: ') !!}
            {!! Form::text('tipo', null, ['class'=>'form-control', 'required']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('descricao', 'Descricao: ') !!}
            {!! Form::text('descricao', null, ['class'=>'form-control', 'required']) !!}
        </div>
        <div class='form-group'>
            {!! Form::submit('Criar Servico', ['class'=>'btn btn-primary'])!!}
            {!! Form::reset('Limpar', ['class'=>'btn btn-default']) !!}
        </div>
    {!! Form::close() !!}
@Stop