@extends('adminlte::page')

@section ('content')
    <h3>Editando Servico: {{ $servico->tipo }}</h3>
    {!! Form::open(['route'=>["servicos.update", 'id'=>$servico->id], 'method'=>'put']) !!}
        <div class="form-group">
            {!! Form::label('tipo', 'Tipo: ') !!}
            {!! Form::text('tipo', $servico->tipo, ['class'=>'form-control', 'required']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('descricao', 'Descricao: ') !!}
            {!! Form::text('descricao',  $servico->descricao, ['class'=>'form-control', 'required']) !!}
        </div>
        <div class='form-group'>
            {!! Form::submit('Editar Ator', ['class'=>'btn btn-primary'])!!}
            {!! Form::reset('Limpar', ['class'=>'btn btn-default']) !!}
        </div>
    {!! Form::close() !!}
@Stop