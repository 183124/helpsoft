@extends('adminlte::page')

@section ('content')
    <h3>Nova Solicitacao</h3>
    {!! Form::open(['route'=>'solicitacoes.store']) !!}
        <div class="form-group">
            {!! Form::label('descricao', 'Descricao: ') !!}
            {!! Form::text('descricao', null, ['class'=>'form-control', 'required']) !!}
        </div>
        <br>
        <div>
            {!! Form::label('gravidade', 'Gravidade: ')!!}
            {!! Form::select('gravidade', 
                array(  'Aguardando Análise'=>'Aguardando Análise'),
              'Aguardando Análise', ['class'=>'form-control', 'required']) !!}
        </div>
        <br>
        <div>
            {!! Form::label('status', 'Status: ')!!}
            {!! Form::select('status', 
                array(  'Aberto'=>'Aberto',
                        ),
              'Aberto', ['class'=>'form-control', 'required']) !!}
        </div>
        <br>
        <div class='form-group'>
            {!! Form::label('data_abertura', 'Data de Abertura: ')!!}
            {!! Form::date('data_abertura', null, ['class'=>'form-control', 'required']) !!}
        </div>
        <div class='form-group'>
            {!! Form::label('data_fechamento', 'Prazo esperado: ')!!}
            {!! Form::date('data_fechamento', null, ['class'=>'form-control', 'required']) !!}
        </div>
        <br>
        <div class='form-group'>
            {!! Form::label('servico_id', 'Serviço: ')!!}
            {!! Form::select('servico_id',
                            \App\Models\Servico::orderBy('tipo')->pluck('tipo', 'id')->toArray(),
                            null, ['class'=>'form-control', 'required']) !!}
        </div>
        <div class='form-group'>
            {!! Form::label('user_id', 'Solicitante: ')!!}
            {!! Form::text('user_id', ($user->id), ['class'=>'form-control', 'readonly']) !!}
        </div>
        </div>
        <br>
        <div class='form-group'>
            {!! Form::submit('Criar Sol', ['class'=>'btn btn-primary'])!!}
            {!! Form::reset('Limpar', ['class'=>'btn btn-default']) !!}
        </div>
        
    {!! Form::close() !!}
@Stop