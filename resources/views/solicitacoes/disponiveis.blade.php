@extends('layouts.default')

@section('content')
<h1>Solicitacoes</h1>
<table class="table table-stripe table-borered table-hover">
    <thead>
        <th>Descricao</th>
        <th>Data de Abertura</th>
        <th>Tipo de Serviço</th>
        <th>Cliente</th>
        <th>Ações</th>
    </thead>
    <tbody>
        @foreach($solicitacoes as $solicitacao)
        <tr>
            <td>{{ $solicitacao->descricao }}</td>
            <td>{{Carbon\Carbon::parse( $solicitacao->data_abertura )->format('d/m/Y')}}</td>    
               
            <td>{{ $solicitacao->servico->tipo }}</td>
            <td>{{ $solicitacao->user->name}}</td>
            <td>
                <a href= "{{ route('solicitacoes.aceitar', ['id'=>$solicitacao->id] ) }}" class="btn-sm btn-success">Aceitar Solicitação</a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@stop

@section('table-delete')
"solicitacoes"
@stop