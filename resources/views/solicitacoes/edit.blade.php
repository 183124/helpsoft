@extends('adminlte::page')

@section ('content')
    <h3>Edição</h3>
    {!! Form::open(['route'=>["solicitacoes.update", 'id' => $solicitacao->id], 'method'=>'put']) !!}
        <div class="form-group">
            {!! Form::label('descricao', 'Descricao: ') !!}
            {!! Form::text('descricao', $solicitacao->descricao, ['class'=>'form-control', 'required']) !!}
        </div>
        <br>
        <div>
            {!! Form::label('gravidade', 'Gravidade: ')!!}
            {!! Form::select('gravidade', 
                array(  'Leve'=>'Leve',
                        'Media'=>'Media',
                        'Grave'=>'Grave',
                        'Critica'=>'Critica'),
              $solicitacao->gravidade, ['class'=>'form-control', 'required']) !!}
        </div>
        <br>
        <div>
            {!! Form::label('status', 'Status: ')!!}
            {!! Form::select('status', 
                array(  'Andamento'=>'Andamento',
                        'Concluido'=>'Concluido',
                        'Encerrado'=>'Encerrado',
                        ),
              'Andamento', ['class'=>'form-control', 'required']) !!}
        </div>
        <br>
        <div class='form-group'>
            {!! Form::label('data_abertura', 'Data de Abertura: ')!!}
            {!! Form::date('data_abertura', $solicitacao->data_abertura, ['class'=>'form-control', 'readonly']) !!}
        </div>
        <div class='form-group'>
            {!! Form::label('data_fechamento', 'Data de Fechamento: ')!!}
            {!! Form::date('data_fechamento', $solicitacao->data_fechamento, ['class'=>'form-control', 'readonly']) !!}
        </div>
        <div class='form-group'>
            {!! Form::label('servico_id', 'Serviço: ')!!}
            {!! Form::select('servico_id',
                            \App\Models\Servico::orderBy('tipo')->pluck('tipo', 'id')->toArray(),
                            $solicitacao->servico_id, ['class'=>'form-control', 'required']) !!}
        </div>
        <br>
        <div class='form-group'>
            {!! Form::submit('Editar Sol', ['class'=>'btn btn-primary'])!!}
            {!! Form::reset('Limpar', ['class'=>'btn btn-default']) !!}
        </div>
    {!! Form::close() !!}
@Stop