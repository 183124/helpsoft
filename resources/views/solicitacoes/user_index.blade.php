@extends('layouts.default')

@section('content')
<h1>Solicitacoes</h1>
<table class="table table-stripe table-borered table-hover">
    <thead>
        <th>Descricao</th>
        <th>Data de Abertura</th>
        <th>Tipo de Serviço</th>
        <th>Status</th>
        <th>Açoes</th>
    </thead>
    <tbody>
        @foreach($solicitacoes as $solicitacao)
        <tr>
            <td>{{ $solicitacao->descricao }}</td>
            <td>{{Carbon\Carbon::parse( $solicitacao->data_abertura )->format('d/m/Y')}}</td>           
            <td>{{ $solicitacao->servico->tipo }}</td>
            <td>{{ $solicitacao->status}}</td>
            <td>
            <a href="{{ route('solicitacoes.view', ['id'=>$solicitacao->id] ) }}" class="btn-sm btn-success">Vizualizar</a>
            <a href= "{{ route('relatos.view', ['id'=>$solicitacao->id] ) }}" class="btn-sm btn-info">Relato</a>
        </td>
        </tr>
        @endforeach
    </tbody>
</table>
<a href= "{{ route('solicitacoes.create', [] ) }}" class="btn-sm btn-info">Adicionar</a>
@stop

@section('table-delete')
"solicitacoes"
@stop