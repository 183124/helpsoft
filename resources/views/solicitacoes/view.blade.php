@extends('layouts.default')

@section('content')

    <h1>Solicitacão </h1>

    <div>
           
        @endphp
        <br>
        <br>
        <br>
        <br>
        <div>
        <h5 style="background-color:darkgray;">Descricao:</h5>
        <h4 >{{ $solicitacao->descricao }}</h4><br>
        </div>
        <h5 style="background-color:darkgray;">Data de Abertura:</h5>
        <h4>{{Carbon\Carbon::parse( $solicitacao->data_abertura )->format('d/m/Y')}}</h4>               <br>    
        <h5 style="background-color:darkgray;">Tipo:</h5>  
        <h4>{{ $solicitacao->servico->tipo }}</h4><br>
        <h5 style="background-color:darkgray;">Status:</h5>
        <h4>{{ $solicitacao->status}}</h4><br>
        <h5 style="background-color:darkgray;">Gravidade:</h5>
        <h4>{{ $solicitacao->gravidade}}</h4><br>
        <h5 style="background-color:darkgray;">Atendente:</h5>
        @php
        use App\Models\User;
        $name = User::where('id', $solicitacao->atendente_id)->get('name');
        @endphp
        <h4>{{ $name }}</h4><br>            
            
        
<a href="{{ route('solicitacoes.indexUser', [] ) }}" class="btn-sm btn-success">Voltar</a>

    </div>
<br>
<br>
<br>
@stop

@section('table-delete')
"solicitacoes"
@stop