@extends('adminlte::page')

@section ('content')
    <h3>Edição de Usuario</h3>
    {!! Form::open(['route'=>["usuarios.update", 'id'=>$usuario->id], 'method'=>'put']) !!}
        <div class="form-group">
            {!! Form::label('name', 'Nome: ') !!}
            {!! Form::text('name', $usuario->name, ['class'=>'form-control', 'required']) !!}
        </div>
        <br>
        <div class='form-group'>
            {!! Form::label('cargo_id', 'Serviço: ')!!}
            {!! Form::select('cargo_id',
                            \App\Models\Cargos::orderBy('descricao')->pluck('descricao', 'id')->toArray(),
                            $usuario->cargo_id, ['class'=>'form-control', 'required']) !!}
        </div>
        <div class='form-group'>
            {!! Form::submit('Confirmar', ['class'=>'btn btn-primary'])!!}
            {!! Form::reset('Limpar', ['class'=>'btn btn-default']) !!}
        </div>
    {!! Form::close() !!}
@Stop