@extends('layouts.default')
@section('content')
<h1>Usuarios</h1>
<table class="table table-stripe table-borered table-hover">
    <thead>
        <th>Nome</th>
        <th>Cargo</th>
        <th>Ações</th>
    </thead>
    <tbody>
        @foreach($usuarios as $usuario)
        <tr>
            <td>
                {{ $usuario->name }}
            </td>
            <td>
                {{ $usuario->cargo_id }}
            </td>
            <td>
                <a href= "{{ route('usuarios.edit', ['id'=>$usuario->id] ) }}" class="btn-sm btn-success">Cargo</a>
            </td>
                
        </tr>
        @endforeach
    </tbody>
</table>
@stop

@section('table-delete')
"usuarios"
@endsection