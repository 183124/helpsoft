<?php

//use App\Http\Controllers\UsuarioController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SolicitacaoController;
use App\Http\Controllers\ServicoController;


Route::group(['middleware' => ['auth', 'can:atendente'], 'prefix' => 'solicitacoes', 'where' => ['id' => '[0-9]+']], function () {
    Route::get('indexAtendente', ['as' => 'solicitacoes.indexAtendente', 'uses' => 'App\Http\Controllers\SolicitacaoController@indexAtendente']);
    Route::post('store', ['as' => 'solicitacoes.store', 'uses' => 'App\Http\Controllers\SolicitacaoController@store']);
    Route::get('{id}/destroy', ['as' => 'solicitacoes.destroy', 'uses' => 'App\Http\Controllers\SolicitacaoController@destroy']);
    Route::get('{id}/edit', ['as' => 'solicitacoes.edit', 'uses' => 'App\Http\Controllers\SolicitacaoController@edit']);
    Route::put('{id}/update', ['as' => 'solicitacoes.update', 'uses' => 'App\Http\Controllers\SolicitacaoController@update']);
    Route::get('create', ['as' => 'relatos.create', 'uses' => 'App\Http\Controllers\RelatoController@create']);
    Route::get('create', ['as' => 'solicitacoes.create', 'uses' => 'App\Http\Controllers\SolicitacaoController@create']);
    Route::get('disponiveis', ['as' => 'solicitacoes.disponiveis', 'uses' => 'App\Http\Controllers\SolicitacaoController@disponiveis']);
    Route::get('{id}/aceitar', ['as' => 'solicitacoes.aceitar', 'uses' => 'App\Http\Controllers\SolicitacaoController@aceitar']);

});

Route::group(['middleware' => ['auth', 'can:atendente'], 'prefix' => 'servicos', 'where' => ['id' => '[0-9]+']], function () {
    Route::get('', ['as' => 'servicos', 'uses' => 'App\Http\Controllers\ServicoController@index']);
    Route::get('create', ['as' => 'servicos.create', 'uses' => 'App\Http\Controllers\ServicoController@create']);
    Route::post('store', ['as' => 'servicos.store', 'uses' => 'App\Http\Controllers\ServicoController@store']);
    Route::get('{id}/destroy', ['as' => 'servicos.destroy', 'uses' => 'App\Http\Controllers\ServicoController@destroy']);
    Route::get('{id}/edit', ['as' => 'servicos.edit', 'uses' => 'App\Http\Controllers\ServicoController@edit']);
    Route::put('{id}/update', ['as' => 'servicos.update', 'uses' => 'App\Http\Controllers\ServicoController@update']);

});


Route::group(['middleware' => ['auth', 'can:atendente'], 'prefix' => 'relatos', 'where' => ['id' => '[0-9]+']], function () {
    Route::get('{id}/mostrar', ['as' => 'relatos.mostrar', 'uses' => 'App\Http\Controllers\RelatoController@mostrar']);
    Route::get('{id}/create', ['as' => 'relatos.create', 'uses' => 'App\Http\Controllers\RelatoController@create']);
    Route::post('store', ['as' => 'relatos.store', 'uses' => 'App\Http\Controllers\RelatoController@store']);
});

Route::group(['middleware' => ['auth', 'can:usuario'], 'prefix' => 'relatos', 'where' => ['id' => '[0-9]+']], function () {
    Route::get('{id}/view', ['as' => 'relatos.view', 'uses' => 'App\Http\Controllers\RelatoController@view']);

});

Route::group(['middleware' => ['auth', 'can:usuario'], 'prefix' => 'avaliacoes', 'where' => ['id' => '[0-9]+']], function () {
    Route::get('', ['as' => 'avaliacoes', 'uses' => 'App\Http\Controllers\AvaliacaoController@index']);
    Route::get('{id}/create', ['as' => 'avaliacoes.create', 'uses' => 'App\Http\Controllers\AvaliacaoController@create']);
    Route::post('store', ['as' => 'avaliacoes.store', 'uses' => 'App\Http\Controllers\AvaliacaoController@store']);

});

Route::group(['middleware' => ['auth', 'can:usuario'], 'prefix' => 'relatorios', 'where' => ['id' => '[0-9]+']], function () {
    Route::get('indexUser', ['as' => 'relatorios.indexUser', 'uses' => 'App\Http\Controllers\PDFController@indexUser']);
});

Route::group(['middleware' => ['auth', 'can:atendente'], 'prefix' => 'relatorios', 'where' => ['id' => '[0-9]+']], function () {
    Route::get('indexAtendente', ['as' => 'relatorios.indexAtendente', 'uses' => 'App\Http\Controllers\PDFController@indexAtendente']);
    Route::get('atendente', ['as' => 'relatorios.atendente', 'uses' => 'App\Http\Controllers\SolicitacaoController@relatorioAtendente']);

});

Route::group([ 'prefix' => 'relatorios', 'where' => ['id' => '[0-9]+']], function () {
    Route::get('notas', ['as' => 'relatorios.notas', 'uses' => 'App\Http\Controllers\SolicitacaoController@relatorioNotas']);
    Route::get('servicos', ['as' => 'relatorios.servicos', 'uses' => 'App\Http\Controllers\SolicitacaoController@relatorioServicos']);
});

Route::group(['middleware' => ['auth', 'can:usuario'], 'prefix' => 'solicitacoes', 'where' => ['id' => '[0-9]+']], function () {
    Route::get('indexUser', ['as' => 'solicitacoes.indexUser', 'uses' => 'App\Http\Controllers\SolicitacaoController@indexUser']);
    Route::post('store', ['as' => 'solicitacoes.store', 'uses' => 'App\Http\Controllers\SolicitacaoController@store']);
    Route::get('{id}/destroy', ['as' => 'solicitacoes.destroy', 'uses' => 'App\Http\Controllers\SolicitacaoController@destroy']);
    Route::get('create', ['as' => 'solicitacoes.create', 'uses' => 'App\Http\Controllers\SolicitacaoController@create']);
    Route::get('{id}/view', ['as' => 'solicitacoes.view', 'uses' => 'App\Http\Controllers\SolicitacaoController@view']);
    

});

Route::group(['middleware' => ['auth', 'can:atendente'], 'prefix' => 'usuarios', 'where' => ['id' => '[0-9]+']], function () {
    Route::get('', ['as' => 'usuarios', 'uses' => 'App\Http\Controllers\UsuarioController@index']);
    Route::get('{id}/destroy', ['as' => 'usuarios.destroy', 'uses' => 'App\Http\Controllers\UsuarioController@destroy']);
    Route::get('{id}/edit', ['as' => 'usuarios.edit', 'uses' => 'App\Http\Controllers\UsuarioController@edit']);
    Route::put('{id}/update', ['as' => 'usuarios.update', 'uses' => 'App\Http\Controllers\UsuarioController@update']);

});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('', [App\Http\Controllers\IndexController::class, 'index'])->name('index');
